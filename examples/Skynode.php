<?php
include('simple_html_dom.php');

  class Skynode {

    public function Filter($text) {

      $html = str_get_html($text);
      $response = array();
      $i = 0;
      foreach($html->find('table.table-dedicated tbody') as $table){
        foreach($table->find('tr') as $tr){
          $response[$i]['cpu'] = $tr->find('td')[0]->innertext;
          $response[$i]['memory'] = intval($tr->find('td')[1]->innertext);
          preg_match('/(?:(\d+) x )?(\d+)/', $tr->find('td')[2]->innertext, $stor);
          $response[$i]['storage'] = sizeof($stor)>2?intval($stor[1])*intval($stor[2]):intval($stor[1]);
          if(strpos($tr->find('td')[2]->innertext,'TB')) $response[$i]['storage'] *= 1000;
          $response[$i]['ssd'] = strpos($tr->find('td')[2]->innertext,'SSD')?1:0;
          $response[$i]['traffic'] = intval($tr->find('td')[4]->innertext);
          $response[$i]['network'] = strpos($tr->find('td')[3]->innertext,'Gbps')?intval($tr->find('td')[3]->innertext)*1000:intval($tr->find('td')[3]->innertext);
          $response[$i]['priceMonthly'] = floatval(preg_replace("/[^-0-9\.]/","",$tr->find('td')[5]->innertext));
          $response[$i]['orderURL'] = 'https://skynode.eu' . $tr->find('td')[7]->find('a')[0]->href;
          $i++;
        }
    
    }
    return $response;
    }
  }
  
?>