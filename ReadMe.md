# metaDedi-Filters

Help us to add more providers to metaDedi.pw

Filters are written in PHP, you can find examples in: examples/

Basically a filter a class, which is loaded dynamically if needed and gets the html code or payload data from the json api.<br />
When everything is parsed, it returns an multidimensional array.

If you like to use regex for that there are some tools you can use:<br />
https://regex101.com/<br />
https://www.phpliveregex.com/

You can also choose a html parser if you like.

To get started, clone this repo, make sure no one else is already working on the filter.<br />
if you see no open issue related to the company, then go ahead, but open an issue so everyone knows someone is already working on it.<br />
Code the filter, submit a merge request.<br />

If you have any questions feel free to ask me.

Explanation of variables used:

| Name          | Info                                              |  Unit | Type               | Example         | Mandatory |
| :------------ | :-----------------------------------------------: | :---: | :----------------: | :-------------: | :-------: |
| cpu           | processor model                                   | n/a   | String             | Atom N2800      | Yes       |
| memory        | memory in GB                                      | GB    | Integer            | 8, 16           | Yes       |
| storage       | storage in TB                                     | TB    | Decimal or Integer | 0.256, 4        | Yes       |
| traffic       | traffic in TB                                     | TB    | Decimal or Integer | 0.5, 20         | Yes       |
| network       | network speed                                     | Mbit  | Decimal or Integer | 1000            | Yes       |
| priceMonthly  | monthly price                                     | n/a   | Decimal            | 4.99, 29.95     | Yes       |
| priceYearly   | monthly price but prepaid yearly                  | n/a   | Decimal            | 4.99, 29.95     | Yes       |
| orderURL      | direct orderURL                                   | n/a   | String             | https://.....   | No        |
| flag          | Override the server location                      | n/a   | String             | Finland         | No        |
| ssd           | Set flag if ssd is available                      | n/a   | Integer            | 1               | No        |

Sites that needed to be added, you can pick one or choose a site thats not listed here by yourself:

https://www.firstheberg.com/en/dedicated-server<br />
